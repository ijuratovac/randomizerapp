﻿namespace RandomizerApp {
    partial class NumberForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.lblFrom = new System.Windows.Forms.Label();
            this.lblTo = new System.Windows.Forms.Label();
            this.btnGenerate = new System.Windows.Forms.Button();
            this.numStart = new System.Windows.Forms.NumericUpDown();
            this.numEnd = new System.Windows.Forms.NumericUpDown();
            this.lblNumber = new System.Windows.Forms.Label();
            this.chbDuplicate = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.numStart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numEnd)).BeginInit();
            this.SuspendLayout();
            // 
            // lblFrom
            // 
            this.lblFrom.AutoSize = true;
            this.lblFrom.Location = new System.Drawing.Point(70, 25);
            this.lblFrom.Name = "lblFrom";
            this.lblFrom.Size = new System.Drawing.Size(38, 15);
            this.lblFrom.TabIndex = 0;
            this.lblFrom.Text = "From:";
            // 
            // lblTo
            // 
            this.lblTo.AutoSize = true;
            this.lblTo.Location = new System.Drawing.Point(76, 78);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(22, 15);
            this.lblTo.TabIndex = 2;
            this.lblTo.Text = "To:";
            // 
            // btnGenerate
            // 
            this.btnGenerate.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnGenerate.Location = new System.Drawing.Point(175, 154);
            this.btnGenerate.Name = "btnGenerate";
            this.btnGenerate.Size = new System.Drawing.Size(85, 31);
            this.btnGenerate.TabIndex = 4;
            this.btnGenerate.Text = "Generate!";
            this.btnGenerate.UseVisualStyleBackColor = true;
            this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
            // 
            // numStart
            // 
            this.numStart.Location = new System.Drawing.Point(37, 43);
            this.numStart.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.numStart.Minimum = new decimal(new int[] {
            999999999,
            0,
            0,
            -2147483648});
            this.numStart.Name = "numStart";
            this.numStart.Size = new System.Drawing.Size(120, 23);
            this.numStart.TabIndex = 5;
            this.numStart.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // numEnd
            // 
            this.numEnd.Location = new System.Drawing.Point(37, 96);
            this.numEnd.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.numEnd.Minimum = new decimal(new int[] {
            999999999,
            0,
            0,
            -2147483648});
            this.numEnd.Name = "numEnd";
            this.numEnd.Size = new System.Drawing.Size(120, 23);
            this.numEnd.TabIndex = 6;
            this.numEnd.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numEnd.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // lblNumber
            // 
            this.lblNumber.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblNumber.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNumber.Font = new System.Drawing.Font("Segoe UI", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblNumber.Location = new System.Drawing.Point(176, 43);
            this.lblNumber.Name = "lblNumber";
            this.lblNumber.Size = new System.Drawing.Size(211, 76);
            this.lblNumber.TabIndex = 7;
            this.lblNumber.Text = "0";
            this.lblNumber.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chbDuplicate
            // 
            this.chbDuplicate.AutoSize = true;
            this.chbDuplicate.Location = new System.Drawing.Point(7, 189);
            this.chbDuplicate.Name = "chbDuplicate";
            this.chbDuplicate.Size = new System.Drawing.Size(121, 19);
            this.chbDuplicate.TabIndex = 8;
            this.chbDuplicate.Text = "Disable duplicates";
            this.chbDuplicate.UseVisualStyleBackColor = true;
            // 
            // NumberForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(434, 211);
            this.Controls.Add(this.chbDuplicate);
            this.Controls.Add(this.lblNumber);
            this.Controls.Add(this.numEnd);
            this.Controls.Add(this.numStart);
            this.Controls.Add(this.btnGenerate);
            this.Controls.Add(this.lblTo);
            this.Controls.Add(this.lblFrom);
            this.Name = "NumberForm";
            this.Text = "Random Number Generator";
            ((System.ComponentModel.ISupportInitialize)(this.numStart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numEnd)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Label lblFrom;
        private Label lblTo;
        private Button btnGenerate;
        private NumericUpDown numStart;
        private NumericUpDown numEnd;
        private Label lblNumber;
        private CheckBox chbDuplicate;
    }
}