﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RandomizerApp {
    public class TypeCompare<T1, T2> {
        public T1 First { get; set; }
        public T2 Second { get; set; }
        public TypeCompare(T1 first, T2 second) {
            First = first;
            Second = second;
        }
        public bool isSameType() {
            if (First!.GetType() == Second!.GetType()) {
                return true;
            }
            return false;
        }
        public class AnonymousInternal {
            public AnonymousInternal() {
                var car = new { Wheels = 4, Manufacturer = "BMW" };
                var bike = new { Wheels = 2, Manufacturer = "BMW" };
                checkWheels(car.Wheels, bike.Wheels);
                checkManufacturer(car.Manufacturer, bike.Manufacturer);
            }
            private void checkWheels(int carWheels, int bikeWheels) {
                if (carWheels == bikeWheels) {
                    MessageBox.Show("Weird");
                }
            }
            private void checkManufacturer(string carManufacturer, string bikeManufacturer) {
                if (carManufacturer.Equals(bikeManufacturer)) {
                    MessageBox.Show("Impressive");
                }
            }
        }
    }
}
