﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RandomizerApp {
    public partial class SecretForm : Form {
        public delegate void Angry();
        public event Angry AngryMessages;
        private int count;
        public SecretForm() {
            InitializeComponent();
            AngryMessages += AngryConversation;
            count = 0;
            TypeCompare<int, double> typeCompare = new(2, 2);
            if (typeCompare.isSameType()) {
                MessageBox.Show("Balance achieved.");
            }
            else {
                MessageBox.Show("Chaos! Chaos! Chaos!");
            }
            TypeCompare<int, int>.AnonymousInternal a = new();
        }
        private void btnSecret_Click(object sender, EventArgs e) {
            if (count == 0) {
                btnSecret.Text = "This is your final warning!";
            }
            if (count == 1) {
                Hide();
                Task task = new Task(Conversation);
                task.Start();
                task.Wait();
                Show();
                btnSecret.Text = "Go away!";
            }
            if (count == 2) {
                AngryMessages();
            }
            count++;
        }
        public static void Conversation() {
            DialogResult dialogResult;
            dialogResult = MessageBox.Show("So...");
            if (dialogResult == DialogResult.OK || dialogResult == DialogResult.Cancel) {
                dialogResult = MessageBox.Show("You pressed the button.");
            }
            if (dialogResult == DialogResult.OK || dialogResult == DialogResult.Cancel) {
                dialogResult = MessageBox.Show("How does it feel...");
            }
            if (dialogResult == DialogResult.OK || dialogResult == DialogResult.Cancel) {
                dialogResult = MessageBox.Show("..doing something you're not supposed to do?");
            }
            if (dialogResult == DialogResult.OK || dialogResult == DialogResult.Cancel) {
                dialogResult = MessageBox.Show("I can forgive you this time.");
            }
            if (dialogResult == DialogResult.OK || dialogResult == DialogResult.Cancel) {
                MessageBox.Show("But don't do it again...");
            }
        }
        public static void AngryConversation() {
            DialogResult dialogResult;
            dialogResult = MessageBox.Show("Hey!");
            if (dialogResult == DialogResult.OK || dialogResult == DialogResult.Cancel) {
                dialogResult = MessageBox.Show("What did I say?!");
            }
            if (dialogResult == DialogResult.OK || dialogResult == DialogResult.Cancel) {
                MessageBox.Show("Stop pressing the button!");
            }
        }
        public static async Task Method1() {
            await Task.Run(() => {
                MessageBox.Show("I'll be watching you...");
            });
        }
        private void SecretForm_FormClosing(object sender, FormClosingEventArgs e) {
            if (count >= 2) {
                Method1();
            }
            else {

            }
        }
    }
}
