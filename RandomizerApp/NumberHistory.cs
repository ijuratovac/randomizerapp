﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace RandomizerApp {
    public class NumberHistory : HistoryAbstract {
        public override string Path { get; set; }
        private ArrayList history;
        public NumberHistory() {
            Path = "./History/Number/number.txt";
            history = new ArrayList();
            Deserialize();
        }
        public override void Serialize() {
            string historyJSON = JsonSerializer.Serialize(history);
            File.WriteAllText(Path, historyJSON);
        }
        public override void Deserialize() {
            try {
                string historyJSON = File.ReadAllText(Path);
                history = JsonSerializer.Deserialize<ArrayList>(historyJSON)!;
            }
            catch {
            }
        }
        public override void ClearHistory() {
            history.Clear();
            Serialize();
        }
        public ArrayList GetReversedHistory() {
            ArrayList reversed = new ArrayList(history);
            reversed.Reverse();
            return reversed;
        }
        public void AddHistory(int n) {
            Deserialize();
            history.Add(n);
            Serialize();
        }
    }
}
