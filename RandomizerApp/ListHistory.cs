﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace RandomizerApp {
    public class ListHistory : HistoryAbstract {
        public override string Path { get; set; }
        private List<string> history;
        public ListHistory() {
            Path = "./History/List/list.txt";
            history = new List<string>();
            Deserialize();
        }
        public override void Serialize() {
            string historyJSON = JsonSerializer.Serialize(history);
            File.WriteAllText(Path, historyJSON);
        }
        public override void Deserialize() {
            try {
                string historyJSON = File.ReadAllText(Path);
                history = JsonSerializer.Deserialize<List<string>>(historyJSON)!;
            }
            catch {
            }
        }
        public override void ClearHistory() {
            history.Clear();
            Serialize();
        }
        public List<string> GetReversedHistory() {
            List<string> reversed = new List<string>(history);
            reversed.Reverse();
            return reversed;
        }
        public void AddHistory(string text) {
            Deserialize();
            history.Add(text);
            Serialize();
        }
    }
}
