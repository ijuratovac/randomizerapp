﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RandomizerApp {
    abstract public class HistoryAbstract {
        abstract public string Path { get; set; }
        abstract public void Serialize();
        abstract public void Deserialize();
        abstract public void ClearHistory();
    }
}
