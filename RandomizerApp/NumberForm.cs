﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RandomizerApp {
    public partial class NumberForm : Form {
        private NumberHistory history;
        private Number Num;
        public NumberForm() {
            InitializeComponent();
            history = new NumberHistory();
            Num = new Number();
        }
        private void btnGenerate_Click(object sender, EventArgs e) {
            Num.Start = Decimal.ToInt32(numStart.Value);
            Num.End = Decimal.ToInt32(numEnd.Value);
            Num.SwapStartEnd();
            Num.NextRandomNumber(Num.Start, Num.End + 1);
            //disable duplicates
            if (chbDuplicate.Checked) {
                Num.NextNonDuplicate();
            }
            lblNumber.Text = Num.Current.ToString();
            history.AddHistory(Num.Current);
        }
    }
}
