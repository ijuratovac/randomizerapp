﻿namespace RandomizerApp {
    partial class StringListForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.lblList = new System.Windows.Forms.Label();
            this.btnGenerate = new System.Windows.Forms.Button();
            this.tbList = new System.Windows.Forms.TextBox();
            this.lblEdit = new System.Windows.Forms.Label();
            this.chbDuplicate = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // lblList
            // 
            this.lblList.AutoEllipsis = true;
            this.lblList.Font = new System.Drawing.Font("Segoe UI", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblList.Location = new System.Drawing.Point(277, 12);
            this.lblList.Name = "lblList";
            this.lblList.Size = new System.Drawing.Size(506, 379);
            this.lblList.TabIndex = 1;
            this.lblList.Text = "Text";
            this.lblList.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnGenerate
            // 
            this.btnGenerate.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnGenerate.Location = new System.Drawing.Point(480, 411);
            this.btnGenerate.Name = "btnGenerate";
            this.btnGenerate.Size = new System.Drawing.Size(108, 32);
            this.btnGenerate.TabIndex = 2;
            this.btnGenerate.Text = "Generate!";
            this.btnGenerate.UseVisualStyleBackColor = true;
            this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
            // 
            // tbList
            // 
            this.tbList.Location = new System.Drawing.Point(12, 29);
            this.tbList.Multiline = true;
            this.tbList.Name = "tbList";
            this.tbList.Size = new System.Drawing.Size(259, 414);
            this.tbList.TabIndex = 3;
            // 
            // lblEdit
            // 
            this.lblEdit.AutoSize = true;
            this.lblEdit.Location = new System.Drawing.Point(12, 11);
            this.lblEdit.Name = "lblEdit";
            this.lblEdit.Size = new System.Drawing.Size(105, 15);
            this.lblEdit.TabIndex = 4;
            this.lblEdit.Text = "Edit the list of text:";
            // 
            // chbDuplicate
            // 
            this.chbDuplicate.AutoSize = true;
            this.chbDuplicate.Location = new System.Drawing.Point(277, 428);
            this.chbDuplicate.Name = "chbDuplicate";
            this.chbDuplicate.Size = new System.Drawing.Size(121, 19);
            this.chbDuplicate.TabIndex = 5;
            this.chbDuplicate.Text = "Disable duplicates";
            this.chbDuplicate.UseVisualStyleBackColor = true;
            // 
            // RandomList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(795, 455);
            this.Controls.Add(this.chbDuplicate);
            this.Controls.Add(this.lblEdit);
            this.Controls.Add(this.tbList);
            this.Controls.Add(this.btnGenerate);
            this.Controls.Add(this.lblList);
            this.Name = "RandomList";
            this.Text = "Random Text Generator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Label lblList;
        private Button btnGenerate;
        private TextBox tbList;
        private Label lblEdit;
        private CheckBox chbDuplicate;
    }
}