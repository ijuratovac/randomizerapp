﻿namespace RandomizerApp {
    partial class RandomizerApp {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.lblChoose = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.numberRandomizerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.textListRandomizerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nothingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.thereIsNothingHereToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.goAwayToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiSecret = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.historyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblChoose
            // 
            this.lblChoose.AutoEllipsis = true;
            this.lblChoose.Font = new System.Drawing.Font("Segoe UI", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lblChoose.Location = new System.Drawing.Point(50, 71);
            this.lblChoose.Name = "lblChoose";
            this.lblChoose.Size = new System.Drawing.Size(253, 53);
            this.lblChoose.TabIndex = 3;
            this.lblChoose.Text = "Randomizer app";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.viewToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(361, 24);
            this.menuStrip1.TabIndex = 4;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.nothingToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.numberRandomizerToolStripMenuItem,
            this.textListRandomizerToolStripMenuItem});
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.openToolStripMenuItem.Text = "Open";
            // 
            // numberRandomizerToolStripMenuItem
            // 
            this.numberRandomizerToolStripMenuItem.Name = "numberRandomizerToolStripMenuItem";
            this.numberRandomizerToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.numberRandomizerToolStripMenuItem.Text = "Number randomizer";
            this.numberRandomizerToolStripMenuItem.Click += new System.EventHandler(this.numberRandomizerToolStripMenuItem_Click);
            // 
            // textListRandomizerToolStripMenuItem
            // 
            this.textListRandomizerToolStripMenuItem.Name = "textListRandomizerToolStripMenuItem";
            this.textListRandomizerToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.textListRandomizerToolStripMenuItem.Text = "Text list randomizer";
            this.textListRandomizerToolStripMenuItem.Click += new System.EventHandler(this.textListRandomizerToolStripMenuItem_Click);
            // 
            // nothingToolStripMenuItem
            // 
            this.nothingToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.thereIsNothingHereToolStripMenuItem});
            this.nothingToolStripMenuItem.Name = "nothingToolStripMenuItem";
            this.nothingToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.nothingToolStripMenuItem.Text = "Nothing";
            // 
            // thereIsNothingHereToolStripMenuItem
            // 
            this.thereIsNothingHereToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.goAwayToolStripMenuItem});
            this.thereIsNothingHereToolStripMenuItem.Name = "thereIsNothingHereToolStripMenuItem";
            this.thereIsNothingHereToolStripMenuItem.Size = new System.Drawing.Size(185, 22);
            this.thereIsNothingHereToolStripMenuItem.Text = "There is nothing here";
            // 
            // goAwayToolStripMenuItem
            // 
            this.goAwayToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiSecret});
            this.goAwayToolStripMenuItem.Name = "goAwayToolStripMenuItem";
            this.goAwayToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.goAwayToolStripMenuItem.Text = "Go away";
            // 
            // tsmiSecret
            // 
            this.tsmiSecret.Name = "tsmiSecret";
            this.tsmiSecret.Size = new System.Drawing.Size(194, 22);
            this.tsmiSecret.Text = "Don\'t press this button";
            this.tsmiSecret.Click += new System.EventHandler(this.tsmiSecret_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // viewToolStripMenuItem
            // 
            this.viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.historyToolStripMenuItem});
            this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
            this.viewToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.viewToolStripMenuItem.Text = "View";
            // 
            // historyToolStripMenuItem
            // 
            this.historyToolStripMenuItem.Name = "historyToolStripMenuItem";
            this.historyToolStripMenuItem.Size = new System.Drawing.Size(112, 22);
            this.historyToolStripMenuItem.Text = "History";
            this.historyToolStripMenuItem.Click += new System.EventHandler(this.historyToolStripMenuItem_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(12, 180);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(173, 21);
            this.label1.TabIndex = 5;
            this.label1.Text = "Made by Ivan Juratovac";
            // 
            // RandomizerApp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(361, 210);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblChoose);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "RandomizerApp";
            this.Text = "Randomizer";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Label lblChoose;
        private MenuStrip menuStrip1;
        private ToolStripMenuItem fileToolStripMenuItem;
        private ToolStripMenuItem openToolStripMenuItem;
        private ToolStripMenuItem numberRandomizerToolStripMenuItem;
        private ToolStripMenuItem textListRandomizerToolStripMenuItem;
        private ToolStripMenuItem exitToolStripMenuItem;
        private ToolStripMenuItem viewToolStripMenuItem;
        private ToolStripMenuItem historyToolStripMenuItem;
        private Label label1;
        private ToolStripMenuItem nothingToolStripMenuItem;
        private ToolStripMenuItem thereIsNothingHereToolStripMenuItem;
        private ToolStripMenuItem goAwayToolStripMenuItem;
        private ToolStripMenuItem tsmiSecret;
    }
}