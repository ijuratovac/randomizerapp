﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RandomizerApp {
    public partial class Number : IDuplicates {
        public Random rng;
        public int Start { get; set; }
        public int End { get; set; }
        public int Previous { get; set; }
        public int Current { get; set; }
        public Number() {
            rng = new Random();
            Previous = 0;
        }
        public void NextNonDuplicate() {
            if (Start == End) {
                return;
            }
            while (Previous == Current) {
                Current = rng.Next(Start, End + 1);
            }
        }
        public void NextRandomNumber(int start, int end) {
            Previous = Current;
            Current = rng.Next(start, end);
        }
        public void SwapStartEnd() {
            if (Start > End) {
                (End, Start) = (Start, End);
            }
        }
    }
}
