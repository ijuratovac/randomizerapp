﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RandomizerApp {
    public partial class StringList : Number, IDuplicates {
        public string PreviousText { get; set; }
        private List<string> list;
        public StringList() {
            list = new List<string>();
            PreviousText = "";
        }
        public bool IsEmpty() {
            if (list.Count > 0) {
                return false;
            }
            return true;
        }
        public int Count() {
            return list.Count;
        }
        public new void NextNonDuplicate() {
            for (int i = 0; PreviousText == list[Current].ToString() && i < 1000000; i++) {
                Current = rng.Next(0, list.Count);
            }
        }
        public void Refresh(TextBox tbList) {
            list.Clear();
            for (int i = 0; i < tbList.Lines.Length; i++) {
                if (tbList.Lines[i] != "") {
                    list.Add(tbList.Lines[i]);
                }
            }
        }
        public string GetText() {
            PreviousText = list[Current].ToString();
            return list[Current].ToString();
        }
    }
}
