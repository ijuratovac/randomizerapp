﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RandomizerApp {
    public partial class HistoryForm : Form {
        public NumberHistory NumberHistory { get; set; }
        public ListHistory ListHistory { get; set; }
        public HistoryForm() {
            InitializeComponent();
            NumberHistory = new NumberHistory();
            ListHistory = new ListHistory();
        }
        public void LoadNumberHistory() {
            lbxNumber.Items.Clear();
            NumberHistory.Deserialize();
            foreach (var item in NumberHistory.GetReversedHistory()) {
                lbxNumber.Items.Add(item);
            }
        }
        public void LoadListHistory() {
            lbxList.Items.Clear();
            ListHistory.Deserialize();
            foreach (var item in ListHistory.GetReversedHistory()) {
                lbxList.Items.Add(item);
            }
        }
        private void btnClearNumbers_Click(object sender, EventArgs e) {
            NumberHistory.ClearHistory();
            LoadNumberHistory();
        }
        private void btnClearList_Click(object sender, EventArgs e) {
            ListHistory.ClearHistory();
            LoadListHistory();
        }
        private void HistoryForm_Shown(object sender, EventArgs e) {
            LoadNumberHistory();
            LoadListHistory();
        }
        private void fswNumber_Changed(object sender, FileSystemEventArgs e) {
            LoadNumberHistory();
        }
        private void fswList_Changed(object sender, FileSystemEventArgs e) {
            LoadListHistory();
        }
    }
}
