namespace RandomizerApp {
    public partial class RandomizerApp : Form {
        public RandomizerApp() {
            InitializeComponent();
            Directory.CreateDirectory("./History");
            Directory.CreateDirectory("./History/Number");
            Directory.CreateDirectory("./History/List");
        }
        private void exitToolStripMenuItem_Click(object sender, EventArgs e) {
            Close();
        }
        private void numberRandomizerToolStripMenuItem_Click(object sender, EventArgs e) {
            NumberForm number = new NumberForm();
            number.Show();
        }
        private void textListRandomizerToolStripMenuItem_Click(object sender, EventArgs e) {
            StringListForm randomList = new StringListForm();
            randomList.Show();
        }
        private void historyToolStripMenuItem_Click(object sender, EventArgs e) {
            HistoryForm historyForm = new HistoryForm();
            historyForm.Show();
        }
        private void tsmiSecret_Click(object sender, EventArgs e) {
            Hide();
            SecretForm secretForm = new SecretForm();
            secretForm.ShowDialog();
            Show();
        }
    }
}