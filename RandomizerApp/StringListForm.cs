﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RandomizerApp {
    public partial class StringListForm : Form {
        private ListHistory history;
        private StringList stringList;
        public StringListForm() {
            InitializeComponent();
            history = new ListHistory();
            stringList = new StringList();
        }
        private void btnGenerate_Click(object sender, EventArgs e) {
            stringList.Refresh(tbList);
            if (stringList.IsEmpty()) {
                lblList.Text = "(The list is empty!)";
            }
            else {
                stringList.NextRandomNumber(0, stringList.Count());
                //disable duplicates
                if (chbDuplicate.Checked) {
                    stringList.NextNonDuplicate();
                }
                lblList.Text = stringList.GetText();
                history.AddHistory(lblList.Text);
            }
        }
    }
}
