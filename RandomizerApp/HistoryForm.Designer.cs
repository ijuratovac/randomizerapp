﻿namespace RandomizerApp {
    partial class HistoryForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.lbxNumber = new System.Windows.Forms.ListBox();
            this.lbxList = new System.Windows.Forms.ListBox();
            this.btnClearNumbers = new System.Windows.Forms.Button();
            this.btnClearList = new System.Windows.Forms.Button();
            this.lblNumberHistory = new System.Windows.Forms.Label();
            this.lblListHistory = new System.Windows.Forms.Label();
            this.fswNumber = new System.IO.FileSystemWatcher();
            this.fswList = new System.IO.FileSystemWatcher();
            ((System.ComponentModel.ISupportInitialize)(this.fswNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fswList)).BeginInit();
            this.SuspendLayout();
            // 
            // lbxNumber
            // 
            this.lbxNumber.FormattingEnabled = true;
            this.lbxNumber.ItemHeight = 15;
            this.lbxNumber.Location = new System.Drawing.Point(12, 28);
            this.lbxNumber.Name = "lbxNumber";
            this.lbxNumber.Size = new System.Drawing.Size(162, 364);
            this.lbxNumber.TabIndex = 0;
            // 
            // lbxList
            // 
            this.lbxList.FormattingEnabled = true;
            this.lbxList.ItemHeight = 15;
            this.lbxList.Location = new System.Drawing.Point(187, 28);
            this.lbxList.Name = "lbxList";
            this.lbxList.Size = new System.Drawing.Size(385, 364);
            this.lbxList.TabIndex = 1;
            // 
            // btnClearNumbers
            // 
            this.btnClearNumbers.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnClearNumbers.Location = new System.Drawing.Point(12, 398);
            this.btnClearNumbers.Name = "btnClearNumbers";
            this.btnClearNumbers.Size = new System.Drawing.Size(162, 29);
            this.btnClearNumbers.TabIndex = 2;
            this.btnClearNumbers.Text = "Clear number history";
            this.btnClearNumbers.UseVisualStyleBackColor = true;
            this.btnClearNumbers.Click += new System.EventHandler(this.btnClearNumbers_Click);
            // 
            // btnClearList
            // 
            this.btnClearList.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnClearList.Location = new System.Drawing.Point(298, 398);
            this.btnClearList.Name = "btnClearList";
            this.btnClearList.Size = new System.Drawing.Size(162, 29);
            this.btnClearList.TabIndex = 3;
            this.btnClearList.Text = "Clear list history";
            this.btnClearList.UseVisualStyleBackColor = true;
            this.btnClearList.Click += new System.EventHandler(this.btnClearList_Click);
            // 
            // lblNumberHistory
            // 
            this.lblNumberHistory.AutoSize = true;
            this.lblNumberHistory.Location = new System.Drawing.Point(12, 9);
            this.lblNumberHistory.Name = "lblNumberHistory";
            this.lblNumberHistory.Size = new System.Drawing.Size(93, 15);
            this.lblNumberHistory.TabIndex = 4;
            this.lblNumberHistory.Text = "Number history:";
            // 
            // lblListHistory
            // 
            this.lblListHistory.AutoSize = true;
            this.lblListHistory.Location = new System.Drawing.Point(187, 9);
            this.lblListHistory.Name = "lblListHistory";
            this.lblListHistory.Size = new System.Drawing.Size(67, 15);
            this.lblListHistory.TabIndex = 5;
            this.lblListHistory.Text = "List history:";
            // 
            // fswNumber
            // 
            this.fswNumber.EnableRaisingEvents = true;
            this.fswNumber.Path = ".\\History\\Number";
            this.fswNumber.SynchronizingObject = this;
            this.fswNumber.Changed += new System.IO.FileSystemEventHandler(this.fswNumber_Changed);
            // 
            // fswList
            // 
            this.fswList.EnableRaisingEvents = true;
            this.fswList.Path = ".\\History\\List";
            this.fswList.SynchronizingObject = this;
            this.fswList.Changed += new System.IO.FileSystemEventHandler(this.fswList_Changed);
            // 
            // HistoryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 450);
            this.Controls.Add(this.lblListHistory);
            this.Controls.Add(this.lblNumberHistory);
            this.Controls.Add(this.btnClearList);
            this.Controls.Add(this.btnClearNumbers);
            this.Controls.Add(this.lbxList);
            this.Controls.Add(this.lbxNumber);
            this.Name = "HistoryForm";
            this.Text = "HistoryForm";
            this.Shown += new System.EventHandler(this.HistoryForm_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.fswNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fswList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ListBox lbxNumber;
        private ListBox lbxList;
        private Button btnClearNumbers;
        private Button btnClearList;
        private Label lblNumberHistory;
        private Label lblListHistory;
        private FileSystemWatcher fswNumber;
        private FileSystemWatcher fswList;
    }
}