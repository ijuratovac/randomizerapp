﻿namespace RandomizerApp {
    partial class SecretForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btnSecret = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnSecret
            // 
            this.btnSecret.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btnSecret.Location = new System.Drawing.Point(53, 50);
            this.btnSecret.Name = "btnSecret";
            this.btnSecret.Size = new System.Drawing.Size(277, 91);
            this.btnSecret.TabIndex = 0;
            this.btnSecret.Text = "Don\'t press this button";
            this.btnSecret.UseVisualStyleBackColor = true;
            this.btnSecret.Click += new System.EventHandler(this.btnSecret_Click);
            // 
            // SecretForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(385, 203);
            this.Controls.Add(this.btnSecret);
            this.Name = "SecretForm";
            this.Text = "SecretForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SecretForm_FormClosing);
            this.ResumeLayout(false);

        }

        #endregion

        private Button btnSecret;
    }
}